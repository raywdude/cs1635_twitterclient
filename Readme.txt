CS 1635 Individual Assignment 4 - Twitter Client application
by Raymond Wang (Gengyu)
gew11@pitt.edu
-------------------------------------------------------------

General Usage:
	1. Install APK to Android device or emulator.
	2. Run application.
	3. Login to Twitter account (automatic)
	4. Home timeline appears.
	5. Scroll through feed if desired.
	6. Mentions button will switch to feed only containing mentions of the user.
	7. Refresh button will refresh the current feed.
	8. Compose button brings up dialog box to allow composing a new tweet.

More Usage:
	- Tapping on tweets that contain URL(s) will launch default Browser and visit the URL.
	  If the API detects a single URL, it will immediately launch the Browser with that URL.
	  If there are multiple URLs, a popup menu containing those URLs will appear.
	- Long press on tweets will display information about the author and provide the
	  opportunity to unfollow the author. Tapping unfollow will show a confirmation screen to allow
	  the user to confirm the action.
	- Open Context menu to perform:
		- Follow: enter a username to follow.
		- Search: sub-menu allows selection of "terms" and "hashtag", then enter word(s) to search for.
		- Edit Personal Info. Will only commit textboxes with changes to Twitter server.
	

Extra Credit:
	- Tweet authors' photos are displayed to the left of the tweet content.
	  For both timelines and search results.
	- Able to follow/unfollow users.
	- Search for both terms and hashtags, accessible via context menu.
	- Edit user personal information, accessible via context menu.

Credits Due:
	Thanks to Fedor Vlasov (a.k.a thest1) for LazyList (displaying images and text in ListView)
	Source: https://github.com/thest1/LazyList