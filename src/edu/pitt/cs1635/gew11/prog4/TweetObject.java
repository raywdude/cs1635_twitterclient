/**
 * CS 1635 Individual Assignment 4
 * by Raymond Wang (Gengyu)
 * gew11@pitt.edu
 * 
 * Twitter Client application
 * 
 * Thanks to Fedor Vlasov (a.k.a thest1) for LazyList (displaying images and text in ListView)
 * (https://github.com/thest1/LazyList)
 * 
 */

package edu.pitt.cs1635.gew11.prog4;

import java.util.Date;

import com.twitterapime.search.Tweet;

public class TweetObject {
	public Tweet tweet;
	public TweetObject(Tweet t) {
		tweet = t;
	}
	
	public String toString() {
		if(tweet.getUserAccount() != null) {
			return 	tweet.getUserAccount().getString("USERACCOUNT_NAME") + " @" + tweet.getUserAccount().getString("USERACCOUNT_USER_NAME") + ":\n" + 
					" " + tweet.getString("TWEET_CONTENT") + "\n" +
					new Date(Long.parseLong(tweet.getString("TWEET_PUBLISH_DATE")));
		}
		else {
			return 	tweet.getString("TWEET_AUTHOR_NAME") + " @" + tweet.getString("TWEET_AUTHOR_USERNAME") + ":\n" + 
					" " + tweet.getString("TWEET_CONTENT") + "\n" +
					new Date(Long.parseLong(tweet.getString("TWEET_PUBLISH_DATE")));
		}
	}
}
