/**
 * CS 1635 Individual Assignment 4
 * by Raymond Wang (Gengyu)
 * gew11@pitt.edu
 * 
 * Twitter Client application
 * 
 * Thanks to Fedor Vlasov (a.k.a thest1) for LazyList (displaying images and text in ListView)
 * (https://github.com/thest1/LazyList)
 * 
 */

package edu.pitt.cs1635.gew11.prog4;

/*
	OAuth settings
	
	Your application's OAuth settings. Keep the "Consumer secret" a secret. This key should never be human-readable in your application.
	
	Access level	 Read and write 
	About the application permission model
	Consumer key	uDt6LiR7qAfV4PtJ3IkBhA
	Consumer secret	juWjnILi5gj6bClPL77zhstDyR9FiJgD06vucubPpI
	Request token URL	https://api.twitter.com/oauth/request_token
	Authorize URL	https://api.twitter.com/oauth/authorize
	Access token URL	https://api.twitter.com/oauth/access_token
	Callback URL	None
	Sign in with Twitter	Yes
	Your access token
	
	Use the access token string as your "oauth_token" and the access token secret as your "oauth_token_secret" to sign requests with your own Twitter account. Do not share your oauth_token_secret with anyone.
	
	Access token	1329307814-BdDx6hL0ULUhwF6mNyZeNUezwar2Hw0k4VJ6o52
	Access token secret	E2MTh2o7NjVCdb7XoLkFYYuoKeQJ3q73uP9ECs2rFA
	Access level	Read and write
  */

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Hashtable;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.fedorvlasov.lazylist.LazyAdapter;
import com.twitterapime.rest.Credential;
import com.twitterapime.rest.Timeline;
import com.twitterapime.rest.TweetER;
import com.twitterapime.rest.UserAccount;
import com.twitterapime.rest.UserAccountManager;
import com.twitterapime.search.InvalidQueryException;
import com.twitterapime.search.LimitExceededException;
import com.twitterapime.search.Query;
import com.twitterapime.search.QueryComposer;
import com.twitterapime.search.SearchDevice;
import com.twitterapime.search.SearchDeviceListener;
import com.twitterapime.search.Tweet;
import com.twitterapime.search.TweetEntity;
import com.twitterapime.xauth.Token;

public class Homescreen extends Activity {
	// Twitter API access credentials
	public static final String TWITTER_USER = "ray_cs1635";
	public static final String ACCESS_TOKEN = "1329307814-BdDx6hL0ULUhwF6mNyZeNUezwar2Hw0k4VJ6o52";
	public static final String ACCESS_SECRET = "E2MTh2o7NjVCdb7XoLkFYYuoKeQJ3q73uP9ECs2rFA";
	public static final String CONSUMER_KEY = "uDt6LiR7qAfV4PtJ3IkBhA";
	public static final String CONSUMER_SECRET = "juWjnILi5gj6bClPL77zhstDyR9FiJgD06vucubPpI";
	
	private ArrayList<TweetObject> listItems;
	private ListView lv;
	//private ArrayAdapter<TweetObject> adapter;
	private LazyAdapter adapter;
	private Credential cred;
	private UserAccountManager uam;
	private Token token;
	private Button refresh;
	private Button homebtn;
	private Button mentionbtn;
	private Button compose;
	
	private boolean lastAccessedHome;
	
	public void makeToast(String s) {
		final String str = s;
		Handler toastHandler = new Handler(Looper.getMainLooper());
		Runnable toastRunnable = new Runnable() {public void run() {Toast.makeText(Homescreen.this.getBaseContext(), str, Toast.LENGTH_SHORT).show();}};
		toastHandler.post(toastRunnable);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_homescreen);
		
		listItems=new ArrayList<TweetObject>();
		lastAccessedHome = true;
		
		compose = (Button)findViewById(R.id.button1);
		compose.setBackgroundResource(R.drawable.compose);
		compose.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				doCompose(v);
			}
		});
		refresh = (Button)findViewById(R.id.Button01);
		refresh.setBackgroundResource(R.drawable.refresh);
		refresh.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				doRefresh(v);
			}
		});
		homebtn = (Button)findViewById(R.id.button2);
		homebtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mentionbtn.setEnabled(true);
				homebtn.setEnabled(false);
				getHomeTimeline(v);
			}
		});
		//homebtn.setBackgroundDrawable(getResources().getDrawable(android.R.drawable.btn_default));
		//homebtn.setBackgroundColor(Color.RED);
		homebtn.setEnabled(false);

		mentionbtn = (Button)findViewById(R.id.button3);
		mentionbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				homebtn.setEnabled(true);
				mentionbtn.setEnabled(false);
				getMentions(v);
			}
		});
		mentionbtn.setEnabled(true);
		lv = (ListView)findViewById(R.id.listView1);
		lv.setOnItemClickListener(new OnItemClickListener() {
			// View information about tweet authors: username, name, number of tweets, number of followers, number of followees.
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				TweetObject tobj = (TweetObject) arg0.getItemAtPosition(arg2);
				if(tobj != null) {
					Tweet tweet = tobj.tweet;
					TweetEntity entity = tweet.getEntity();
					if(entity != null) {
						TweetEntity [] urls = entity.getURLs();
						// Make sure there's URLs to process in the first place
						if(urls != null && urls.length > 0) {
							// One URL. Just launch it.
							if(urls.length == 1) {
								String strurl = urls[0].getString("TWEETENTITY_URL");
								try {
									// Use Intent to launch browser with URL
									Intent intent = Intent.parseUri(strurl, 0);
									startActivity(intent);
								} catch (URISyntaxException e) {
									e.printStackTrace();
								}
							}
							// More than one, so let user choose which one they want to open
							else {
								// Pop up menu for user to select URL
								final ArrayList<String> listItems = new ArrayList<String>();
								ArrayAdapter<String> adapter = new ArrayAdapter<String>(Homescreen.this, android.R.layout.simple_list_item_1, listItems);
								AlertDialog.Builder alert = new AlertDialog.Builder(Homescreen.this);                 
							    alert.setTitle("Select URL to open"); 
							    alert.setAdapter(adapter, new DialogInterface.OnClickListener() {  
							    	public void onClick(DialogInterface dialog, int item) {
							    		if(item >= 0 && item < listItems.size()) {
							    			try {
												Intent intent = Intent.parseUri(listItems.get(item), 0);
												startActivity(intent);
											} catch (URISyntaxException e) {
												e.printStackTrace();
											}
							    		}
							    		return;
							    	}
							     });

							    // Add a cancel button!
							    alert.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {  
							    	public void onClick(DialogInterface dialog, int item) {
							    		return;
							    	}
							     });

							    // Populate the list
								for(TweetEntity url : urls) {
									String strurl = url.getString("TWEETENTITY_URL");
									listItems.add(strurl);
									adapter.notifyDataSetChanged();
								}
								
							    // Show the dialog
							    alert.show();
							}
						}
					}
				}
			}
		});
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				TweetObject tobj = (TweetObject) arg0.getItemAtPosition(arg2);
				// Get information about the tweet author and display it
				if(tobj != null) {
					DecimalFormat formatter = new DecimalFormat("#,###");
					final Tweet tweet = tobj.tweet;
					if(tweet.getUserAccount() == null) {
						return true;
					}
					AlertDialog.Builder alert = new AlertDialog.Builder(Homescreen.this);                 
				    alert.setTitle("About @" + tweet.getUserAccount().getString("USERACCOUNT_USER_NAME") + "'s twitter account");  
				    alert.setMessage("Name: " + tweet.getUserAccount().getString("USERACCOUNT_NAME") + "\n" +
				    "Number of tweets: " + formatter.format(Long.parseLong(tweet.getUserAccount().getString("USERACCOUNT_TWEETS_COUNT"))) + "\n" +
				    "Number of followers: " + formatter.format(Long.parseLong(tweet.getUserAccount().getString("USERACCOUNT_FOLLOWERS_COUNT"))) + "\n" +
				    "Number of followees: " + formatter.format(Long.parseLong(tweet.getUserAccount().getString("USERACCOUNT_FRIENDS_COUNT"))));  
				    alert.setNegativeButton("Unfollow", new DialogInterface.OnClickListener() {  
				    	public void onClick(DialogInterface dialog, int whichButton) {
				    		AlertDialog.Builder confirm = new AlertDialog.Builder(Homescreen.this);
				    		confirm.setTitle("Confirm");
				    		confirm.setMessage("Are you sure you want to unfollow @" + tweet.getUserAccount().getString("USERACCOUNT_USER_NAME") + "?");
				    		confirm.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
						    		new Thread() {
						    			public void run() {
								    		try {
												uam.unfollow(tweet.getUserAccount());
												makeToast("Unfollowed @" + tweet.getUserAccount().getString("USERACCOUNT_USER_NAME") + ".");
											} catch (IOException e) {
												e.printStackTrace();
												makeToast("Unfollow failed: network disconnected.");
											} catch (LimitExceededException e) {
												e.printStackTrace();
												makeToast("Unfollow failed: Too many user actions.");
											}
						    			}
						    		}.start();
								}
				    		});
				    		confirm.setNegativeButton("No", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
						    		return;
								}
				    		});
				    		confirm.show();
				    		return;
				    	}
				     });
				    alert.setPositiveButton("Close", new DialogInterface.OnClickListener() {  
				    	public void onClick(DialogInterface dialog, int whichButton) {
				    		return;
				    	}
				     });
				    alert.show();
				}
				return true;
			}

		});
		//adapter = new ArrayAdapter<TweetObject>(this, android.R.layout.simple_list_item_1, listItems);
		adapter = new LazyAdapter(this, listItems);
		lv.setAdapter(adapter);
		
		token = new Token(ACCESS_TOKEN, ACCESS_SECRET);
		cred = new Credential(TWITTER_USER, CONSUMER_KEY, CONSUMER_SECRET, token);
		 
		uam = UserAccountManager.getInstance(cred);
		try {
			if(uam.verifyCredential()) {
				makeToast("Verified " + TWITTER_USER + "!");
			}
			else {
				makeToast("Verification failed!\n" + uam.toString());
				return;
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (LimitExceededException e1) {
			e1.printStackTrace();
		}
		
		this.getHomeTimeline(this.getCurrentFocus());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.homescreen, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.searchterms: // Search terms
			doSearch("terms");
			break;
		case R.id.searchhashtags: // Search hashtags
			doSearch("hashtag");
			break;
		case R.id.follow: // follow user
			doFollow();
			break;
		case R.id.editinfo: // Edit personal info
			doEditPersonal();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void doEditPersonal() {
		final StringBuilder user_name = new StringBuilder("");
		final StringBuilder user_desc = new StringBuilder("");
		final StringBuilder user_url = new StringBuilder("");
		final StringBuilder user_location = new StringBuilder("");
		
		AlertDialog.Builder alert = new AlertDialog.Builder(this);                 
	    alert.setTitle("Edit Personal Info");

	    final EditText name = new EditText(Homescreen.this.getApplicationContext());
	    final EditText desc = new EditText(Homescreen.this.getApplicationContext());
	    final EditText url = new EditText(Homescreen.this.getApplicationContext());
	    final EditText location = new EditText(Homescreen.this.getApplicationContext());
	    final LinearLayout layout = new LinearLayout(Homescreen.this.getApplicationContext());
	    layout.setOrientation(LinearLayout.VERTICAL);
	    name.setTextColor(Color.BLACK);
	    name.setHint(getString(R.string.str_full_name));//"Name");
	    desc.setTextColor(Color.BLACK);
	    desc.setHint(getString(R.string.str_description));//"Description");
	    url.setTextColor(Color.BLACK);
	    url.setHint(getString(R.string.str_url));//"Website URL");
	    location.setTextColor(Color.BLACK);
	    location.setHint(getString(R.string.str_location));//"Location");

	    layout.addView(name);
	    layout.addView(desc);
	    layout.addView(url);
	    layout.addView(location);
	    
	    /*** RETREIVE PERSONAL INFO FROM ACCOUNT ***/
		new Thread() {
			public void run() {
			    UserAccount user = null;
				try {
					user = uam.getUserAccount();
					user_name.append(user.getString("USERACCOUNT_NAME"));
					user_desc.append(user.getString("USERACCOUNT_DESCRIPTION"));
					user_url.append(user.getString("USERACCOUNT_URL"));
					user_location.append(user.getString("USERACCOUNT_LOCATION"));
				} catch (IOException e) {
					e.printStackTrace();
					return;
				} catch (LimitExceededException e) {
					e.printStackTrace();
					return;
				}
				
				Handler toastHandler = new Handler(Looper.getMainLooper());
				Runnable toastRunnable = new Runnable() {
					public void run() {
					    name.setText(user_name);
					    desc.setText(user_desc);
					    url.setText(user_url);
					    location.setText(user_location);
					}
				};
				toastHandler.post(toastRunnable);
			}
		}.start();
	    /*******************************************/
	    
	    alert.setView(layout);
	    
	    alert.setPositiveButton("Set", new DialogInterface.OnClickListener() {  
		    public void onClick(DialogInterface dialog, int whichButton) {
		    	final Hashtable<String,String> d = new Hashtable<String,String>(4);
		    	if(!user_name.toString().equals(name.getText().toString())) {
		    		d.put("USERACCOUNT_NAME", name.getText().toString());
		    	}
		    	if(!user_desc.toString().equals(desc.getText().toString())) {
		    		d.put("USERACCOUNT_DESCRIPTION", desc.getText().toString());
		    	}
		    	if(!user_url.toString().equals(url.getText().toString())) {
		    		d.put("USERACCOUNT_URL", url.getText().toString());
		    	}
		    	if(!user_location.toString().equals(location.getText().toString())) {
		    		d.put("USERACCOUNT_LOCATION", location.getText().toString());
		    	}
		    	
		    	// something to set
		    	if(d.size() > 0) {
		    		new Thread() {
		    			public void run() {
		    				try {
		    					UserAccount newInfo = new UserAccount(d);
		    					newInfo = uam.updateProfile(newInfo);
		    					if(newInfo != null) {
		    						makeToast("Update successful. (" + d.size() + ")");
		    					}
		    					else {
		    						makeToast("Update failure.");
		    					}
		    				} catch (IOException e) {
		    					e.printStackTrace();
		    					return;
		    				} catch (LimitExceededException e) {
		    					e.printStackTrace();
		    					return;
		    				}
		    			}
		    		}.start();
		    	}
		    	else {
		    		makeToast("No fields were changed.");
		    	}
		    }
	    });
	    
	    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {  
		    public void onClick(DialogInterface dialog, int whichButton) {
		    	return;
		    }
	    });
	    
	    alert.show();
	}
	
	public void doSearch(final String searchType) {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);                 
	    alert.setTitle("Search");  
	    alert.setMessage("Enter " + searchType + " to search for:");        
	    final EditText query = new EditText(Homescreen.this.getApplicationContext());
	    query.setTextColor(Color.BLACK);
	    alert.setView(query);

	    alert.setPositiveButton("Search", new DialogInterface.OnClickListener() {  
	    public void onClick(DialogInterface dialog, int whichButton) {
	    	final String strquery = query.getText().toString();
	    	if(strquery.length() > 0) {
				Handler updateListHandler = new Handler(Looper.getMainLooper());
				Runnable updateListRunnable = new Runnable() {
					public void run() {
			    		listItems.clear();
			    		adapter.notifyDataSetChanged();
			    		
			    		// enable buttons since we are not in either mode
						homebtn.setEnabled(true);
						mentionbtn.setEnabled(true);
					}
				};
				updateListHandler.post(updateListRunnable);

	    		new Thread() {
	    			public void run() {
	    			    try {
	    			    	Tweet [] tweets = null;
    			    		SearchDevice s = SearchDevice.getInstance();
    			    		Query q = null;
	    			    	if(searchType.equals("hashtag")) {
	    			    		q = QueryComposer.resultCount(20);
	    			    		q = QueryComposer.append(QueryComposer.containHashtag(strquery), q);
	    			    	}
	    			    	else {
	    			    		q = QueryComposer.containAll(strquery);
	    			    	}
	    			    	tweets = s.searchTweets(q);
	    			    	if(tweets != null && tweets.length > 0) {
	    			    		for(final Tweet tweet : tweets) {
	    			    			if(tweet != null) {
	    								Handler searchUpdateHandler = new Handler(Looper.getMainLooper());
	    								Runnable searchUpdateRunnable = new Runnable() {
	    									public void run() {
	    										listItems.add(new TweetObject(tweet));
	    										adapter.notifyDataSetChanged();
	    									}
	    								};
	    								searchUpdateHandler.post(searchUpdateRunnable);
	    			    			}
	    			    		}
	    			    		
	    			    		makeToast("Found " + tweets.length + " tweets matching \"" + strquery + "\"");
	    			    	}
	    			    	else {
	    			    		makeToast("No tweets found matching \"" + strquery + "\"");
	    			    	}

	    				}
	    			    catch (Exception e) { 
	    			    	e.printStackTrace();
	    			    }
	    			}
	    		}.start();
	    	}
	    	else {
	    		makeToast("Please enter something to search for.");
	    	}
	        return;
	       }
	     });  

	    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	            return;   
	        }
	    });
	    alert.show();
	}
	
	public void doFollow() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);                 
	    alert.setTitle("Follow User");  
	    alert.setMessage("Enter username to follow:");        
	    final EditText username = new EditText(Homescreen.this.getApplicationContext());
	    username.setTextColor(Color.BLACK);
	    alert.setView(username);

	    alert.setPositiveButton("Follow", new DialogInterface.OnClickListener() {  
	    public void onClick(DialogInterface dialog, int whichButton) {
	    	final String strusername = username.getText().toString();
	    	if(strusername.length() > 0) {
	    		new Thread() {
	    			public void run() {
	    				UserAccount ua = new UserAccount(strusername);
	    				try {
							uam.follow(ua);
							makeToast("Followed @" + strusername + ".");
						} catch (IOException e) {
							e.printStackTrace();
							makeToast("Follow failed: network disconnected.");
						} catch (LimitExceededException e) {
							e.printStackTrace();
							makeToast("Follow failed: Too many user actions.");
						} catch (InvalidQueryException e) {
							e.printStackTrace();
							makeToast("Follow failed: User @" + strusername + " not found.");
						} catch(Exception e) {
							makeToast("Follow failed: Other - " + e);
						}
	    			}
	    		}.start();
	    	}
	    	else {
	    		makeToast("Please enter a username to follow.");
	    	}
	        return;
	       }
	     });  

	    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	            return;   
	        }
	    });
	    alert.show(); 
	}
	
	public void getHomeTimeline(View v) {
		lastAccessedHome = true;
		listItems.clear();
		adapter.notifyDataSetChanged();
		new Thread() {
			public void run() {
			    try {
					Timeline tml = Timeline.getInstance(uam);
					tml.startGetHomeTweets(QueryComposer.includeEntities(), new SearchDeviceListener() {
						int numtweets = 0;

						public void tweetFound(final Tweet tweet) {
							numtweets++;
							Handler toastHandler = new Handler(Looper.getMainLooper());
							Runnable toastRunnable = new Runnable() {
								public void run() {
									//listItems.add("Tweet received: " + tweet);
									listItems.add(new TweetObject(tweet));
									adapter.notifyDataSetChanged();
								}
							};
							toastHandler.post(toastRunnable);
						}

						@Override
						public void searchCompleted() {
							makeToast(numtweets + " tweets found!");
						}	

						@Override
						public void searchFailed(Throwable arg0) {
							makeToast("Search failed! Please try again later.");
						}
					});

				}
			    catch (Exception e) { 
			    	e.printStackTrace();
			    }
			}
		}.start();
	}
	
	public void getMentions(View v) {
		lastAccessedHome = false;
		listItems.clear();
		adapter.notifyDataSetChanged();
		new Thread() {
			public void run() {
			    try {
					Timeline tml = Timeline.getInstance(uam);
					tml.startGetMentions(QueryComposer.includeEntities(), new SearchDeviceListener() {
						int numtweets = 0;
						//http://stackoverflow.com/questions/4540754/add-dynamically-elements-to-a-listview-android
						
						public void tweetFound(final Tweet tweet) {
							numtweets++;
							Handler toastHandler = new Handler(Looper.getMainLooper());
							Runnable toastRunnable = new Runnable() {
								public void run() {
									
									//listItems.add("Tweet received: " + tweet);
									listItems.add(new TweetObject(tweet));
									adapter.notifyDataSetChanged();
								}
							};
							toastHandler.post(toastRunnable);
						}

						@Override
						public void searchCompleted() {
							makeToast(numtweets + " tweets found!");
						}	

						@Override
						public void searchFailed(Throwable arg0) {
							makeToast("Search failed! Please try again later.");
						}
					});

				}
			    catch (Exception e) { 
			    	e.printStackTrace();
			    }
			}
		}.start();
	}
	
	public void postTweet(final String message) {
		new Thread() {
			public void run() {
				Tweet t = new Tweet(message);
				TweetER ter = TweetER.getInstance(uam);
				try {
					ter.post(t);
			    	new Handler(Looper.getMainLooper()).post(
			    			new Runnable() {
				    				public void run() {
				    					getHomeTimeline(Homescreen.this.getCurrentFocus());
				    					makeToast("Tweet posted.");
				    				}
			    			}
			    		);
				} catch (IOException e) {
					makeToast("Connection error.");
				} catch (LimitExceededException e) {
					makeToast("Maximum number of identical tweets exceeded!");
				}
			}
		}.start();
	}
	
	public void doCompose(final View v) {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);                 
	    alert.setTitle("Compose new tweet");  
	    alert.setMessage("Enter your message:");        
	    final EditText message = new EditText(Homescreen.this.getApplicationContext());
	    message.setTextColor(Color.BLACK);
	    alert.setView(message);

	    alert.setPositiveButton("Post", new DialogInterface.OnClickListener() {  
	    public void onClick(DialogInterface dialog, int whichButton) {
	    	String strpost = message.getText().toString();
	    	if(strpost.length() > 0) {
	    		postTweet(strpost);
	    	}
	    	else {
	    		makeToast("Please enter a message before attempting to post.");
	    	}
	        return;
	       }
	     });  

	    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	            return;   
	        }
	    });
	    alert.show(); 

	}
	
	public void doRefresh(final View v) {
		if(lastAccessedHome) {
			getHomeTimeline(v);
		}
		else {
			getMentions(v);
		}
	}
}
