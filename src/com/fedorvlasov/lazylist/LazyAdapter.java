// Fedor Vlasov's LazyAdapter, modified by Raymond Wang to use TweetObject ArrayList for handling twitter feeds
// Source: https://github.com/thest1/LazyList

package com.fedorvlasov.lazylist;

import edu.pitt.cs1635.gew11.prog4.R;
import edu.pitt.cs1635.gew11.prog4.TweetObject;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

public class LazyAdapter extends BaseAdapter {
    
    private Activity activity;
    private ArrayList<TweetObject> tweetObj;
    private static LayoutInflater inflater=null;
    public ImageLoader imageLoader; 
    
    public LazyAdapter(Activity a, ArrayList<TweetObject> to) {
        activity = a;
        tweetObj=to;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader=new ImageLoader(activity.getApplicationContext());
    }

    public int getCount() {
        return tweetObj.size();
    }

    public Object getItem(int position) {
        return tweetObj.get(position);
    }

    public long getItemId(int position) {
        return position;
    }
    
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.item, null);

        TextView text=(TextView)vi.findViewById(R.id.text);;
        ImageView image=(ImageView)vi.findViewById(R.id.image);
        text.setText(tweetObj.get(position).toString());
        if(tweetObj.get(position).tweet.getUserAccount() != null) {
        	imageLoader.DisplayImage(tweetObj.get(position).tweet.getUserAccount().getString("USERACCOUNT_PICTURE_URI"), image);
        }
        else {
        	imageLoader.DisplayImage(tweetObj.get(position).tweet.getString("TWEET_AUTHOR_PICTURE_URI"), image);
        }
        return vi;
    }
}